$(function(){
	"use strict";
	ImgChange();
	ImgHoverChange();
	HeaderHeight();
	//SPMenuHeight();
	//MenuHeight();
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	var flg = 'default';
	$(document).on("click","#toggle, #global-nav .p-close", function() {
	//$( "#toggle" ).click( function(){

		$( "#toggle-btn-icon" ).toggleClass( "close" );

		if( flg === "default" ){
			
			flg = "close";

			$( "#global-nav-area" ).addClass( "show" );
			//setTimeout(function(){
//				$('.global-list li a').matchHeight();
//			},10);

		}else{
			
			flg = "default";
			$( "#global-nav-area" ).removeClass( "show" );
			//$('.global-list li a').off();
		}

		$("#global-nav-area").slideToggle('slow');
	});
	
	//$( ".js-global-nav-detail" ).hide();
	//$( ".js-underlayer" ).click(
//		function(){
//			$(this).next().toggleClass( "show" );
//	
//			if( $(this).next().hasClass( "show" ) ){
//				
//				$(this).find(".js-menu").text( "―" );
//				$(this).next().addClass( "show" );
//	
//			}else{
//				
//				$(this).find(".js-menu").text( "＋" );
//				$(this).next().removeClass( "show" );
//			}
//	
//			$(this).next().slideToggle('slow');
//		}
//	);
	
	var topBtn = $('#page-top');    
	topBtn.hide();
	
	//スクロールが500に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
		
		//TOP以外のページの時にスクロールが一番上にある時ヘッダーの背景の透明度を無しにする。
		if($('#top').length){
			
		} else {
			
			//if ($(window).scrollTop() < 75) {
			//	$(".l-header").css("background-color","rgba(0,0,0,1.0)");
			//} else {
			//	$(".l-header").css("background-color","rgba(0,0,0,0.5)");
			//}
		}
	});
	
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1500);
		return false;
	});
	
	$(window).on('load', function(){		
		var widimage = window.innerWidth;		
		if( widimage < 1001 ){
			setTimeout(function(){
				var speed = 10;
				var urlHash = location.hash;
				var target2 = $(urlHash);
				//別ページからの遷移時
				if (urlHash) {
					var position2 = target2.offset().top - $('.l-header').height();
					$("html, body").animate({scrollTop:position2}, speed, "swing");
					return false;
				}
			},10);
		}
	});	
	
	$(window).on('load resize', function(){
		ImgChange();
		setTimeout(function(){
			SPMenuHeight();
		},100);
	});
		
	$('a[href^=#]').click(function(){	
		if($('#company').length){
			
		} else {
			var speed = 1000;
			var href= $(this).attr("href");
			var target = $(href === "#" || href === "" ? 'html' : href);
			var position = 0;
			
			if($(this).hasClass('is-pagescroll')){
				position = target.offset().top - $('.l-header').height();				
				$("html, body").animate({scrollTop:position}, speed, "swing");
				return false;
			} else if($(this).hasClass('is-concept')){
			
			} else {
				position = target.offset().top;			
				$("html, body").animate({scrollTop:position}, speed, "swing");
				return false;
			}
		}
	});
	
	$(".p-info").hover(
		function(){
			//alert('a');
			$(this).find('.header_courses_menu').addClass('on_hover');
		},
		function () {
			$(this).find('.header_courses_menu').removeClass('on_hover');
		}
	);

});


function  ImgChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1601 ){
		$('.is-imgChange0').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
		
		$('.is-imgChange0').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 769 ){
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 481 ){
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 1001 ){
		if( widimage < 769 ){
			$('.is-message').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_pc', '_sp2'));
			});
		} else {
			$('.is-imgChange').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
			});
		}
	} else {
	
		$('.is-message').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp2', '_sp'));
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}

}
function  ImgHoverChange(){
	"use strict";
	
	$('.is-hoverChange').hover(function(){
		$(this).attr("src",$(this).attr("src").replace('_off', '_on'));		
	},function(){
		$(this).attr("src",$(this).attr("src").replace('_on', '_off'));		
	});
	
	var fadeSpeed = 300;
	var rolloverImg = $('.is-hoverChange2');
	rolloverImg.each(function() { 
	
		//ブラウザサイズによる画像の切り替え
		var widimage = window.innerWidth;
		if( widimage < 1001 ){
			$('.is-hoverChange2').each( function(){
				//$(this).attr("src",$(this).attr("src").replace('_off', '_on'));
				//$(this).attr("src",$(this).attr("src").replace('_on', '_off'));
			});
		} else {
			//フェードインで画像を切り替え
			
			if(this.src.match('_off')) {
			var imgWidth = $(this).css('width');
			var imgHeight = 'auto';
				$(this).parent('a').css( {display: 'block', width: imgWidth + 'px'});
				 
				this.onImgSrc = new Image();
				this.onImgSrc.src = this.getAttribute('src').replace('_off', '_on'); 
				$(this.onImgSrc).css( {position: 'absolute', opacity: 0, width: imgWidth + 'px', height: 'auto'} ); 
				$(this).before(this.onImgSrc);
				 
				$(this.onImgSrc).mousedown(function(){ 
					$(this).stop().animate({opacity: 0}, {duration: fadeSpeed, queue: false}); 
				}); 
		 
				$(this.onImgSrc).hover(
					function(){ $(this).animate( {opacity: 1}, {duration: fadeSpeed, queue: false}); },
					function(){ $(this).animate( {opacity: 0}, {duration: fadeSpeed, queue: false}); }
				);
			}
		}
	});
	
	var fadeSpeed2 = 200;
	var rolloverImg2 = $('.is-hoverChange3');
	rolloverImg2.each(function() { 
	
		//ブラウザサイズによる画像の切り替え
		var widimage = window.innerWidth;
		if( widimage < 1001 ){
			$('.is-hoverChange3').each( function(){
				//$(this).attr("src",$(this).attr("src").replace('_off', '_on'));
				//$(this).attr("src",$(this).attr("src").replace('_on', '_off'));
			});
		} else {
			//フェードインで画像を切り替え
			if(this.src.match('_off')) {
			var imgWidth2 = $(this).css('width');
			var imgHeight2 = 'auto';
				$(this).parent('a').css( {display: 'block', width: imgWidth2 + 'px', float: 'left',marginLeft:35 + 'px', verticalAlign:'top'});
				 
				this.onImgSrc2 = new Image();
				this.onImgSrc2.src = this.getAttribute('src').replace('_off', '_on'); 
				$(this.onImgSrc2).css( {position: 'absolute', opacity: 0, width: imgWidth2 + 'px', height: 'auto'} ); 
				$(this).before(this.onImgSrc2);
				 
				$(this.onImgSrc2).mousedown(function(){ 
					$(this).stop().animate({opacity: 0}, {duration: fadeSpeed2, queue: false}); 
				}); 
		 
				$(this.onImgSrc2).hover(
					function(){ $(this).animate( {opacity: 1}, {duration: fadeSpeed2, queue: false}); },
					function(){ $(this).animate( {opacity: 0}, {duration: fadeSpeed2, queue: false}); }
				); 
			}
		}
	});
}

function  HeaderHeight(){
	"use strict";
	var header_Height = $('header').height();
	//$('.l-mvBlock').css('marginTop',header_Height + 'px');
}
function  SPMenuHeight(){
	"use strict";
	var header_Height = $('header').height();
	var menu_Height = 0;
	if(navigator.userAgent.indexOf('Android') > 0){
        menu_Height = window.innerHeight - 100;
    } else {
		menu_Height = window.innerHeight - header_Height;
	}
	//var menu_Height = window.innerHeight - 150;
	$('#global-nav-area').css('top',header_Height + 'px');
	$('#global-nav-area').css('height',menu_Height + 'px');
	$('#global-nav-area').css('overflow-y','auto');
	$('#global-nav-area').css('-webkit-overflow-scrolling','touch');
}


